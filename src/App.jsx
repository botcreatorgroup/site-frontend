import React from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './App.scss';

import Navbar from './components/layout/Navbar/Navbar.jsx';
import Auth from './components/auth/Auth/Auth.jsx';
import BotsList from './components/bots/list/BotsList/BotsList.jsx';
import ProtectedRoute from './components/routes/ProtectedRoute.jsx';
import Alert from "./components/layout/Alert/Alert.jsx";
import Settings from "./components/bots/Settings/Settings.jsx";

const App = () => {
  const alert = useSelector(state => state.app.alert);

  return (
    <Router>
      <div className='App'>
        <Navbar />
        {alert && <Alert alert={alert}/>}
        <Switch>
          <ProtectedRoute component={BotsList} path='/bots' />
          <Route path='/settings'>
            <Settings />
          </Route>
          <Route path='/'>
            <Auth />
          </Route>
        </Switch>
      </div>
    </Router>
  )
};

export default App;
