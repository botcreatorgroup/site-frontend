import axios from 'axios';
import jwt from 'jsonwebtoken';
import Cookies from 'js-cookie';

import {
  REMOVE_USER,
  LOGIN_USER,
  REGISTER_USER,
  LOAD_BOTS,
  RESTORE_USER,
  SET_ALERT,
  REMOVE_ALERT,
  SET_LOADING,
  REMOVE_LOADING
} from "./types";

// App actions

export const setAlert = alert => {
  return dispatch => {
    dispatch({ type: SET_ALERT, payload: alert });
    setTimeout(() => dispatch({ type: REMOVE_ALERT }), 3000);
  }
}

export const setLoading = () => ({ type: SET_LOADING });
export const removeLoading = () => ({ type: REMOVE_LOADING });

// Auth actions

export const loginUser = (userData) => {
  return async dispatch => {
    dispatch(setLoading());

    // TODO Remove the timer
    setTimeout(async () => {
      try {
        await axios.post('http://localhost:3001/api/v1/auth/login', userData, { withCredentials: true });

        const token = Cookies.get('X-Auth-Token');
        const user = await jwt.decode(token);

        dispatch({
          type: LOGIN_USER,
          payload: user
        });
      } catch (e) {
        const msg = e.response.data.message || 'Error';
        dispatch(setAlert({ msg }));
      }

      dispatch(removeLoading());
    }, 1000);
  }
};

export const registerUser = (userData) => {
  return async dispatch => {
    dispatch(setLoading());

    try {
      await axios.post('http://localhost:3001/api/v1/auth/register', userData, { withCredentials: true });

      const token = Cookies.get('X-Auth-Token');
      const user = await jwt.decode(token);

      dispatch({
        type: REGISTER_USER,
        payload: user
      });
    } catch(e) {
      const msg = e.response.data.message || 'Error';
      dispatch(setAlert({ msg }));
    }

    dispatch(removeLoading());
  }
};

export const restoreUser = () => {
  console.log('Launch');
  const token = Cookies.get('X-Auth-Token');

  if (token) {
    const user = jwt.decode(token);

    return {
      type: RESTORE_USER,
      user
    };
  }

  return { type: 'DEFAULT' };
};

export const removeUser = () => {
  Cookies.remove('X-Auth-Token');

  return {
    type: REMOVE_USER
  }
};

// Bots actions

export const loadBots = () => {
  return async dispatch => {
    dispatch(setLoading());

    try {
      const { data } = await axios.get('http://localhost:3001/api/v1/bots', { withCredentials: true });

      dispatch({
        type: LOAD_BOTS,
        payload: data.map(bot => ({ ...bot, image: './telegram.svg' }))
      });
    } catch(e) {
      console.log(e.message);
    }

    dispatch(removeLoading());
  }
}

export const createBot = (botData) => {
  return async dispatch => {
    dispatch(setLoading());

    try {
      await axios.post('http://localhost:3001/api/v1/bots', botData, { withCredentials: true });

      dispatch(loadBots());
    } catch(e) {
      console.log(e.message);
    }

    dispatch(removeLoading());
  }
}
