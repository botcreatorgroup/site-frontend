import { combineReducers } from 'redux';

import appReducer from './appReducer';
import authReducer from './authReducer';
import botsReducer from './botsReducer';

export default combineReducers({
  app: appReducer,
  auth: authReducer,
  bots: botsReducer
});
