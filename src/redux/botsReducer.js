import { LOAD_BOTS } from "./types";

const initialState = {
  list: []
};

const botsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_BOTS:
      return { ...state, list: action.payload }
    default:
      return state;
  }
}

export default botsReducer;
