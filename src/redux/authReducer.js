import {LOGIN_USER, REGISTER_USER, REMOVE_USER, RESTORE_USER} from "./types";

const initialState = {
  isAuthenticated: false,
  user: null
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER:
    case REGISTER_USER:
    case RESTORE_USER:
      return { ...state, isAuthenticated: true, user: action.payload }
    case REMOVE_USER:
      return { ...state, isAuthenticated: false, user: null }
    default:
      return state;
  }
}

export default authReducer;
