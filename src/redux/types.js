// AUTH
export const LOGIN_USER = 'LOGIN_USER';
export const REGISTER_USER = 'REGISTER_USER';
export const RESTORE_USER = 'RESTORE_USER';
export const REMOVE_USER = 'REMOVE_AUTH';

// BOTS
export const LOAD_BOTS = 'LOAD_BOTS';

// APP
export const SET_LOADING = 'SET_LOADING';
export const REMOVE_LOADING = 'REMOVE_LOADING';
export const SET_ALERT = 'SET_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';


