import {REMOVE_ALERT, REMOVE_LOADING, SET_ALERT, SET_LOADING} from "./types";

const initialState = {
  alert: null,
  loading: false
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ALERT:
      return { ...state, alert: action.payload }
    case REMOVE_ALERT:
      return { ...state, alert: null }
    case SET_LOADING:
      return { ...state, loading: true }
    case REMOVE_LOADING:
      return { ...state, loading: false }
    default:
      return state;
  }
};

export default appReducer;


