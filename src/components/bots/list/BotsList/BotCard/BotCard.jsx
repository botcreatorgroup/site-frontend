import React from 'react';

const BotCard = props => {
  const { title, image, isRunning } = props;

  return (
    <div className='card bg-light m-2' style={{ width: '350px' }}>
      <div className='card-header' style={{ height: '60px' }}>
        <h4 className='card-title text-center'>{title} <span className={isRunning ? 'badge badge-success' : 'badge badge-danger'}>{isRunning ? 'In Run' : 'Stopped'}</span></h4>
      </div>
      <div className='card-body' style={{ height: '350px' }}>
        <img className='card-img' src={image} alt='Telegram Logo'/>
      </div>
      <div className='card-footer d-flex justify-content-around' style={{ height: '70px' }}>
        <button className={isRunning ? 'btn btn-success m-1 disabled' : 'btn btn-success m-1'}>Run bot</button>
        <button className={isRunning ? 'btn btn-danger m-1' : 'btn btn-danger m-1 disabled'}>Stop bot</button>
        <button className='btn btn-dark m-1'>Settings</button>
      </div>
    </div>
  )
};

export default BotCard;
