import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import BotCard from './BotCard/BotCard.jsx';
import CreateCard from '../CreateCard/CreateCard.jsx';
import CreateCardForm from '../CreateCardForm/CreateCardForm.jsx';
import { loadBots } from '../../../../redux/actions';
import Spinner from '../../../layout/Spinner/Spinner';

const BotsList = () => {
  const dispatch = useDispatch();
  useEffect(() => dispatch(loadBots()), []);

  const bots = useSelector(state => state.bots.list);
  const isLoading = useSelector(state => state.app.loading);

  const [isCreating, setIsCreating] = useState(false);

  return (
    <div style={{ position: 'relative' }}>
      {/*TODO Set spinner to the top*/}
      { isLoading && <Spinner /> }
    <div className='container-fluid bg-secondary' style={{ height: '60px', padding: '10px' }}>
      <h1 className='text-center text-white'>Bots</h1>
    </div>
    <div className='container mt-3'>
      <div className='d-flex justify-content-start flex-wrap'>
        {bots.map(({ title, image, isRunning }) => <BotCard title={title} image={image} isRunning={isRunning}/>)}
        {isCreating ? <CreateCardForm setIsCreating={setIsCreating}/> : <CreateCard setIsCreating={setIsCreating}/>}
      </div>
    </div>
    </div>
  )
};

export default BotsList;
