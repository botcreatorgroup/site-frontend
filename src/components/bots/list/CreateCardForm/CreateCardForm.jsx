import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createBot } from '../../../../redux/actions';
import Spinner from '../../../layout/Spinner/Spinner';

const CreateCardForm = ({ setIsCreating }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.app.loading);

  const [bot, setBot] = useState({ title: '', token: '' });
  const { title, token } = bot;

  const onChange = ({ target }) => setBot({ ...bot, [target.name]: target.value });

  const onSubmit = e => {
    e.preventDefault();

    dispatch(createBot(bot));
    setBot({ title: '', token: '' });
  }

  return (
  <div className='card bg-light m-2' style={{ position: 'relative', width: '350px' }}>
    { isLoading && <Spinner /> }
    <div className='card-header d-flex justify-content-between align-items-center' style={{ height: '60px' }}>
      <h4>Create a new bot</h4>
      <button className='btn btn-danger' onClick={() => setIsCreating(false)} style={{ zIndex: '10' }}><i className='fas fa-times'></i></button>
    </div>
    <form onSubmit={onSubmit}>
    <div className='card-body' style={{ height: '350px' }}>
        <div className='form-group'>
          <label>Messenger</label>
          <select className='custom-select'>
            <option selected>Choose the messenger</option>
            <option>Telegram</option>
            <option disabled>Viber</option>
            <option disabled>Facebook</option>
          </select>
        </div>
        <div className='form-group'>
          <label>Title</label>
          <input className='form-control' type='text' name='title' value={title} onChange={onChange}/>
        </div>
        <div className='form-group'>
          <label>Token</label>
          <input className='form-control' type='text' name='token' value={token} onChange={onChange}/>
        </div>
    </div>
    <div className='card-footer' style={{ height: '70px' }}>
      <button className='btn btn-block btn-success' type='submit'>Create</button>
    </div>
    </form>
  </div>
  );
};

export default CreateCardForm;


