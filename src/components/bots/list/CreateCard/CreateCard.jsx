import React, { useState } from 'react';

const CreateCard = ({ setIsCreating }) => {
  return (
    <div className='card bg-light m-2' style={{ width: '350px' }}>
      <div className='card-header' style={{ height: '60px' }}>
        <h4 className='card-title text-center'>Create a new bot</h4>
      </div>
      <div className='card-body'>
        <img className='card-img' src='./plus.svg' alt='Add bot' style= {{ cursor: 'pointer' }} onClick={() => setIsCreating(true)}/>
      </div>
      <div className='card-footer' style={{ height: '70px' }}>
      </div>
    </div>
  );
}

export default CreateCard;
