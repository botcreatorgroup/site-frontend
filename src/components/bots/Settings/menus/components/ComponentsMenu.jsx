import React, { useState } from 'react';

import SchemaList from './SchemaList/SchemaList.jsx';
import ComponentList from './TabComponentList/TabComponentList';

import './ComponentsMenu.scss';

const ComponentsMenu = () => {
  const [isSchemaMenu, setIsSchemaMenu] = useState(true);

  return (
    <div className='components-menu'>
      <div className='button-group'>
        <button className='btn btn-light menu-button' onClick={() => setIsSchemaMenu(true)}>Schemas</button>
        <button className='btn btn-light menu-button' onClick={() => setIsSchemaMenu(false)}>Components</button>
      </div>
      {isSchemaMenu ? <SchemaList /> : <ComponentList />}
    </div>
  );
};

export default ComponentsMenu;
