import React from 'react';

import TabComponent from './TabComponent/TabComponent';

import { TextFields, Image, Description, Audiotrack, Movie, RadioButtonChecked } from '@material-ui/icons';
import './TabComponentList.scss';

const SchemaList = () => {
  const components = [{ title: 'Text', image: TextFields }, { title: 'Document', image: Description }, { title: 'Image', image: Image }, { title: 'Audio', image: Audiotrack }, { title: 'Video', image: Movie }, { title: 'Button', image: RadioButtonChecked }];

  return (
    <div className='tab-components-list'>
      {components.map(c => <TabComponent title={c.title} image={c.image}/>)}
    </div>
  );
};

export default SchemaList;

