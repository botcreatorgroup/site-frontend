import React from 'react';

import './TabComponent.scss';

const TabComponent = ({ image: ImageComponent, title }) => {
  return (
    <div className='card tab-component'>
      <div className='card-body tab-component-body'>
        <ImageComponent fontSize='large'/>
        <span className='tab-component-title'>{title}</span>
      </div>
    </div>
  )
};

export default TabComponent;

