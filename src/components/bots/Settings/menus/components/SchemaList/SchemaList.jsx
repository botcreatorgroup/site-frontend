import React from 'react';

import Schema from './Schema/Schema.jsx';

import './SchemaList.scss';

const SchemaList = () => {
  const schemas = [{ title: 'StartSchema' }, { title: 'Schema1'}, { title: 'EndSchema' }, { title: 'StartSchema' }, { title: 'Schema1'}, { title: 'EndSchema' }];
  return (
    <div className='schema-list'>
      {schemas.map(s => <Schema title={s.title}/>)}
    </div>
  );
};

export default SchemaList;

