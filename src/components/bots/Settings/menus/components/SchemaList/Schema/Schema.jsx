import React from 'react';

import './Schema.scss';

const Schema = ({ title }) => {
  return (
    <div className='card schema'>
      <div className='card-header schema-header'>
        <p className='card-text'>{title}</p>
      </div>
      <div className='card-body schema-body'>
        <button className='btn'>Edit schema</button>
        <button className='btn'>Remove schema</button>
      </div>
    </div>
  )
};

export default Schema;
