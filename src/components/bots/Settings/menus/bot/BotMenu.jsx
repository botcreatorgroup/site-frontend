import React from 'react';

import './BotMenu.scss';

const BotMenu = () => {
  return (
    <div className='bot-menu'>
      <form className='image-form'>
        <div className='form-group'>
          <label className='text-white'>Change bot`s image</label>
          <img className='img-thumbnail' src='facebook.svg' alt='Bot image'/>
          <div className='upload'>
            <input type='file'/>
            <p>Click or drag your image here to update</p>
          </div>
        </div>
        <button className='btn btn-light'>Upload</button>
      </form>
      <form className='title-form'>
        <div className='form-group'>
          <label className='text-white'>Change bot`s title</label>
          <input className='form-control' type='text'/>
        </div>
        <button className='btn btn-light'>Change title</button>
      </form>
      <form className='token-form'>
        <div className='form-group'>
          <label className='text-white'>Change a token</label>
          <input className='form-control' type='text'/>
        </div>
        <button className='btn btn-light'>Change token</button>
      </form>
    </div>
  );
};

export default BotMenu;
