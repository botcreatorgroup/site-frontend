import React from 'react';

import BotMenu from './menus/bot/BotMenu.jsx';
import ComponentsMenu from './menus/components/ComponentsMenu.jsx';
import SchemaMenu from './menus/schema/SchemaMenu';

import './Settings.scss';

const Settings = () => {
  return (
    <div className='wrapper'>
      <BotMenu />
      <SchemaMenu />
      <ComponentsMenu />
    </div>
  );
};

export default Settings;
