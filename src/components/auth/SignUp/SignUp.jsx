import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { registerUser } from "../../../redux/actions";

const SignUp = () => {
  const dispatch = useDispatch();
  const [user, setUser] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: ''
  });

  const { firstName, lastName, email, password, confirmPassword } = user;

  const onChange = ({ target }) => setUser({ ...user, [target.name]: target.value });

  const onSubmit = async e => {
    e.preventDefault();

    const userData = {
      email,
      firstName,
      lastName,
      password
    };

    dispatch(registerUser(userData));
  }

  return (
    <div className='container'>
      <form onSubmit={onSubmit}>
        <div className='form-group'>
          <label>First Name</label>
          <input className='form-control' type='text' name='firstName' value={firstName} onChange={onChange}/>
        </div>
        <div className='form-group'>
          <label>Last Name</label>
          <input className='form-control' type='text' name='lastName' value={lastName} onChange={onChange}/>
        </div>
        <div className='form-group'>
          <label>Email</label>
          <input className='form-control' type='email' name='email' value={email} onChange={onChange}/>
        </div>
        <div className='form-group'>
          <label>Password</label>
          <input className='form-control' type='password' name='password' value={password} onChange={onChange}/>
        </div>
        <div className='form-group'>
          <label>Confirm Password</label>
          <input className='form-control' type='password' name='confirmPassword' value={confirmPassword} onChange={onChange}/>
        </div>
        <button type='submit' className='btn btn-block btn-lg btn-danger'>SignUp</button>
      </form>
    </div>
  );
};

export default SignUp;
