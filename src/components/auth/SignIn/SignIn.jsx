import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {loginUser} from "../../../redux/actions";

const SignIn = () => {
  const dispatch = useDispatch();
  const [user, setUser] = useState({
    email: '',
    password: ''
  });

  const { email, password } = user;

  const onChange = ({ target }) => setUser({ ...user, [target.name]: target.value });

  const onSubmit = async e => {
    e.preventDefault();

    dispatch(loginUser(user));
  };

  return (
    <div className='container'>
      <form onSubmit={onSubmit}>
        <div className='form-group'>
          <label>Email</label>
          <input className='form-control' name='email' type='email' value={email} onChange={onChange}/>
        </div>
        <div className='form-group'>
          <label>Password</label>
          <input className='form-control' name='password' type='password' value={password} onChange={onChange}/>
        </div>
        <button type='submit' className='btn btn-block btn-lg btn-success'>SignIn</button>
      </form>
    </div>
  );
};

export default SignIn;
