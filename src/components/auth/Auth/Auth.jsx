import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { restoreUser } from "../../../redux/actions";
import SignIn from '../SignIn/SignIn.jsx';
import SignUp from '../SignUp/SignUp.jsx';

import './Auth.scss';
import Spinner from "../../layout/Spinner/Spinner";

const Auth = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  const isLoading = useSelector(state => state.app.loading);

  const [isLogin, setIsLogin] = useState(true);

  useEffect(() => {
    if (isAuthenticated)
      history.push('/bots');
    else
      dispatch(restoreUser());
  }, [isAuthenticated, history, dispatch]);


  return (
    <div className='container-fluid auth'>
      <div className='row'>
        <div className='col-md-6 my-2'>
            <img className='' src='main-logo.png' alt='' style={{ height: '500px', width: '655px', marginLeft: '50px' }}/>
        </div>
        <div className='col-md-1'></div>
        <div className='col-md-4'>
          <ul className='pagination pagination-lg justify-content-center mb-2'>
            <li className={`page-item ${isLogin ? 'active' : ''}`}>
              <button className="page-link" onClick={() => setIsLogin(true)}>SignIn</button>
            </li>
            <li className={`page-item ${isLogin ? '' : 'active'}`}>
              <button className="page-link" onClick={() => setIsLogin(false)}>SignUp</button>
            </li>
          </ul>
          <div className='auth-form'>
            {isLogin? <SignIn/> : <SignUp/>}
            {isLoading && <Spinner />}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Auth;
