import React from 'react';

import './Spinner.scss';

const Spinner = () => {
  return (
    <div className='overlay'>
      <div className="cssload-loader"></div>
    </div>
  )
}

export default Spinner;
