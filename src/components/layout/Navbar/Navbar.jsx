import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { removeUser } from "../../../redux/actions";

import './Navbar.scss';

const Navbar = () => {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

  const onClick = e => dispatch(removeUser());

  return (
    <nav>
      <div style={{ display: 'flex', justifyContent: 'space-between'}}>
      <span className='icon'>BotCreatorProject</span>
        {isAuthenticated && (<button className='btn btn-dark' onClick={onClick}><span style={{ fontFamily: 'Helvetica' }}>Logout</span></button>)}
      </div>
    </nav>
  );
};

export default Navbar;
