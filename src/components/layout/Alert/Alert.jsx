import React from 'react';

const Alert = ({ alert }) => {
  const { msg } = alert;

  return (
    <div className='alert bg-danger m-3'>
      <i className="fas fa-exclamation-circle text-white" style={{ fontSize: '18px' }}><span> {msg}</span></i>
    </div>
  )
}

export default Alert;
